<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/menu.php');
require_once($DELIBDIR.'/php/cms/gateway.php');
require_once("$DELIBDIR/php/nan/form.php");
require_once($DELIBDIR.'/php/nan/table.php');
require_once($DELIBDIR.'/php/navigator.php');
$subm =new DecomMenu();
$subdept=new DecomMenu();
$subperm=new DecomMenu();
$subedit=new DecomMenu();
$subuser=new DecomMenu();
switch($_SESSION['utype'])
{	
	case 'hod':  
							$subedit->addItem(
								new DecomMenuItem(_('EditProfile'),_('index.php?page=editprofileForm'),_('editprofile')));
							$subedit->addItem(
	   						new DecomMenuItem(_('Add Field'),_('index.php?page=field'),_('add field')));
							$subuser->addItem(
	   						new DecomMenuItem(_('Add User'),_('index.php?page=adduserForm'),_('add user')));
							$subdept->addItem(
								new DecomMenuItem('EditDepartment','index.php?page=editdeptForm', 'editdept'));
	   	   			$subm->addItem(
	  						new DecomMenuItem(_('Profile'), _('index.php?page=profile'), _('myprofile'),$subedit));
							$subm->addItem(
	  						new DecomMenuItem(_('People'), _('index.php?page=people'), _('people')));
							$subm->addItem(
	   						new DecomMenuItem(_('Users'),_('index.php?page=listuser'),_('usersdetails'),$subuser));
							$subm->addItem(
	   						new DecomMenuItem(_('Department'),_('index.php?page=viewdept'),_('departmentdetails'),$subdept));
							break;
	case 'staff':$subedit->addItem(
								new DecomMenuItem(_('EditProfile'),_('index.php?page=editprofileForm'),_('editprofile')));
								$subm->addItem(
	  						new DecomMenuItem(_('Profile'), _('index.php'), _('myprofile'),$subedit));
								$subm->addItem(
	  						new DecomMenuItem(_('People'), _('index.php?page=people'), _('people')));
								$subm->addItem(
	   						new DecomMenuItem(_('Department'),_('index.php?page=viewdept'),_('departmentdetails')));   
			   			 
							 break;
	case 'faculty':$subedit->addItem(
								new DecomMenuItem(_('EditProfile'),_('index.php?page=editprofileForm'),_('editprofile')));
								$subm->addItem(
	  						new DecomMenuItem(_('Profile'), _('index.php'), _('myprofile'),$subedit));
								$subm->addItem(
	  						new DecomMenuItem(_('People'), _('index.php?page=people'), _('people')));
								$subm->addItem(
	   						new DecomMenuItem(_('Department'),_('index.php?page=viewdept'),_('departmentdetails')));	   
							 	//$con .="faculty profile";
							 	break;
	case 'admin':
				   		 $subperm->addItem(
	   						new DecomMenuItem(_('Active/Inactive'),_('index.php?page=activation'),_('activation'))); 
	  					 $subdept->addItem(
	   						new DecomMenuItem(_('Add Field'),_('index.php?page=field'),_('add field')));
							 $subdept->addItem(
								new DecomMenuItem('EditDepartment','index.php?page=dept', 'editdept'));
							 $subdept->addItem(
								new DecomMenuItem('AddDepartment','index.php?page=adddeptForm', 'adddept'));
							 $subdept->addItem(
								new DecomMenuItem('EditSchool','index.php?page=school', 'editschool'));
							 $subdept->addItem(
								new DecomMenuItem('AddSchool','index.php?page=addschoolForm', 'addschool'));
							 $subuser->addItem(
	   						new DecomMenuItem(_('Add User'),_('index.php?page=adduserForm'),_('add user')));
							 $subm->addItem(
	   						new DecomMenuItem(_('Users'),_('index.php?page=listuser'),_('usersdetails'),$subuser));
							 $subm->addItem(
	   						new DecomMenuItem(_('Department'),_(''),_('departmentdetails'),$subdept));
								$subm->addItem(
	   						new DecomMenuItem(_('Permission Settings'),_(''),_('permission'),$subperm));
							  break;
	default:	$subedit->addItem(
								new DecomMenuItem(_('EditProfile'),_('index.php?page=editprofileForm'),_('editprofile')));
								$subm->addItem(
	  						new DecomMenuItem(_('Profile'), _('index.php'), _('myprofile'),$subedit));
}

if(in_array($_SESSION['utype'], ['hod','faculty','staff','admin','others'])){
		$nav =  new DecomMenu();
		$nav->addItem(
	   	new DecomMenuItem('Home', '/index.php', 'Home page'));
		$nav->addItem(
	   	new DecomMenuItem('Contact', 'index.php?page=contact', 'Contact details'));
		decom_page_set_side_menu($subm);
		
	 	//$con .=file_get_contents($_SERVER['DOCUMENT_ROOT'].'/../include/editor.html');
}
decom_page_set_navbar($nav);
$page = isset($_GET['page'])? $_GET['page']: 'home';
$ret  = decom_autoinclude($page, '../include-auto/'.$_SESSION['utype']);
if(is_a($ret, 'DecomError')) {
	decom_page_add_error_message($ret->getMessageHtml());
}
else {
	foreach($ret as $path)
		include($path);
}
?>

