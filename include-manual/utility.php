<?php
/* utility.php
 * Copyright (C) 2019 Ishaque*/
require_once($DELIBDIR.'/php/people/person.php');
function ik_user_get_dept_id($eid)
{
    $p = new DecomPerson($eid);
    if(is_a($p, 'DecomError')){
				$con .= '<p>Error creating entity: '.$ret->getMessageHtml().'</p>';
    }
    else{
        $did = $p->getRelativeFirst('head_of');
        if($did === NULL|| $did === FALSE ) {
            $did = $p->getRelativeFirst('faculty_of');
            if($did === NULL|| $did === FALSE ) {
                $did = $p->getRelativeFirst('staff_of');
                if($did === NULL|| $did === FALSE ) {
                    return NULL;
                }
                else {
                    return $did;
                }
            }
            else {
                return $did;
            }
        }
        else {
            return $did;
        }
    }

}

function ik_get_person_fullname($eid)
{
$pid = new DecomPerson($eid);
$fullname = [];
$fullname=$pid->getPropertyValue('first_name').' '.$pid->getPropertyValue('middle_name').$pid->getPropertyValue('last_name');
return $fullname;
}
