<?php
if(!isset($_POST['school'])) {
	decom_page_add_error_message(_('Entity ID not specified.'), _('Error deleting entity'));
}
else {
	$ret = decom_remove_entity('institute',$_POST['school']);
	if($ret === true)
		decom_page_add_message(_('Entity deleted successfully.'));
	else
		decom_page_add_error_message($ret->getMessageHtml(), _('Error deleting entity'));
}
?>  
