<?php
if(!isset($_POST['inst'])) {
	decom_page_add_error_message(_('Entity ID not specified.'), _('Error deleting entity'));
}
else {
	$ret = decom_remove_entity('institute',$_POST['inst']);
	if($ret === true)
		decom_page_add_message(_('Entity deleted successfully.'));
	else
		decom_page_add_error_message($ret->getMessageHtml(), _('Error deleting entity'));
}
?> 
