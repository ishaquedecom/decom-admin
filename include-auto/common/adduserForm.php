<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/menu.php');
require_once($DELIBDIR.'/php/class.php');
require_once("$DELIBDIR/php/nan/form.php");
require_once($DELIBDIR.'/php/people/person.php');
		decom_page_set_title('Register Now!');
   	$con.='<fieldset>';
    $inputFields= ['uname', 'passwd', 'passwdConfirm'];
		$inputFieldLabels = ['Userame', 'Password', 'Confirm Password'];
		$inputFieldsReq   = [true, true, true];

		if(!isset($acts[2])) {
			$inputFieldTypes  = ['text', 'password', 'password'];
			$action = '?page=adduserProcess';
			$method = 'POST';
			$con .= nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method);
		}
?>
