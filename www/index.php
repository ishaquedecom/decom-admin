<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once($DELIBDIR.'/php/views/page.php');
require_once($DELIBDIR.'/php/menu.php');
require_once($DELIBDIR.'/php/nan/form.php');
require_once($DELIBDIR.'/php/views/entity.php');
session_start();
decom_page_init();
$con = '';
if (!isset($_SESSION['utype'])) { // Not logged in
	decom_page_set_title(_('Login'));
	$frm  = nan_generate_form(['uname','passwd'],['Username','Password'],['text', 'password'],[true,true],"auth.php");
	$con .= $frm;
}
else {                            // Logged in
	$con .='<p>Logged in as'.' '.$_SESSION['uname'].' '.'<a href="?page=logout"><button>Logout</button></a>';
	include($_SERVER['DOCUMENT_ROOT'].'/../include-manual/logged-in-user.php');
}

decom_page_set_content($con);
decom_page_display();
?>

