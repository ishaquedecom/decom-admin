 <?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once($DELIBDIR.'/php/entity.php');

	/* Assumptions:
	 *  There is an object of the class 'institute' with ID = 50
	 *  There is an object of the class 'person' with ID = 6
	 *  There is a relationship 'head_of' from person to institute.
	 *  person(6).head_of is set to 50.
	 */

	$i = new DecomEntity('institute', 50);
	$head = $i->getInverseRelativeFirst('head_of_inst');

	$p = new DecomEntity('person', 28);
	$headOf = $p->getRelativeFirst('head_of_inst');
	echo "<p>Person(28) is the head of Institute($headOf).</p>";
	echo "<p>Institute(100) has Person($head) as the head.</p>";
?>
